import * as actionTypes from '../constants/actionTypes';

const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SORT_SET:
      return setSort(state, action);
  }
  return state;
}

function setSort(state, action) {
  const { sort } = action;
  return sort;
}

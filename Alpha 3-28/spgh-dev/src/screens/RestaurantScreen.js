import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';

import SwappingComponentsView from '../components/SwappingComponentsView';
import RestaurantImage from '../components/RestaurantImage';
import RestaurantDetails from '../components/RestaurantDetails';
import RestaurantScreenHeader from '../components/RestaurantScreenHeader';
import LatoText from '../components/StyledText';


class RestaurantScreen extends React.Component
{  
  /*Main render function*/
  render() //Build application
  {
	const { params } = this.props.navigation.state;
    return (
		<View style={{ flex: 1, }}>
		  <RestaurantScreenHeader
			 title={params.restaurant.name}
			 navigation={this.props.navigation}
			 style={{ flex: 1, }}
		  />
          <RestaurantImage uri={params.restaurant.image}/>
		  <SwappingComponentsView headerText={params.restaurant.name}
							   primarySwapText="See full assessment"
							   secondarySwapText="Hide assessment"
							   renderPrimaryComponent={this.renderPrimaryComponent}
							   renderSecondaryComponent={this.renderSecondaryComponent}
							   primaryStyle={{alignSelf: 'stretch'}}
							   secondaryStyle={{alignSelf: 'stretch', backgroundColor: '#70abad'}}
							   params={params}>
		   </SwappingComponentsView>
		</View>
    );
  }
  
  /*Component render functions*/
  renderPrimaryComponent(params)
  {
	 return (
		<ScrollView contentContainerStyle={styles.scrollContainer}>
			<RestaurantDetails restaurant={params.restaurant} />
		</ScrollView>
	  );
  }
  renderSecondaryComponent(params)
  {	
	return (
		<View style={{backgroundColor: '#70abad'}}>
			<Text style={styles.secondaryTextContainer}>General: {params.restaurant.score.general}</Text>
			<Text style={styles.secondaryTextContainer}>Employee Actions: {params.restaurant.score.employee_actions}</Text>
			<Text style={styles.secondaryTextContainer}>Energy Efficiency: {params.restaurant.score.energy_efficiency}</Text>
			<Text style={styles.secondaryTextContainer}>Nutrition: {params.restaurant.score.nutrition}</Text>
			<Text style={styles.secondaryTextContainer}>People: {params.restaurant.score.people}</Text>
			<Text style={styles.secondaryTextContainer}>Responsible Sourcing: {params.restaurant.score.responsible_sourcing}</Text>
			<Text style={styles.secondaryTextContainer}>Waste Reduction: {params.restaurant.score.waste_reduction}</Text>
			<Text style={styles.secondaryTextContainer}>Water Conservation: {params.restaurant.score.water_conservation}</Text>
		</View>
	);
  }
}

export default RestaurantScreen;

const styles = StyleSheet.create({
  scrollContainer: {
	justifyContent: 'center',
  },
  secondaryTextContainer: {
	padding: 5,
	color: '#fff',
	fontSize: 19,
  },
});
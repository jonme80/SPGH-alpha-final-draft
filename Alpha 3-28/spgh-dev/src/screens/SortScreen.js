import React from 'react';

import {
    ActivityIndicator,
    Button,
    ListView,
    RefreshControl,
    StyleSheet,
    Text,
    TextInput,
    View,
    FlatList,
    TouchableHighlight
} from 'react-native';
import { AppLoading } from 'expo';
import { NavigationActions } from 'react-navigation';

import _ from 'lodash';
import { connect } from 'react-redux';
import {batchActions} from 'redux-batched-actions';


import SortOptions from '../constants/SortOptions';
import SortOptionList from '../components/SortOptionList';
import AppText from '../constants/AppText';
import RouteNames from '../constants/RouteNames';
import BackHeader from '../components/BackHeader';
import * as actions from '../actions';

class SortScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
    };

    onPressItem = (item) => {
        this.props.setSort(item);
    };

    constructor(props) {
        super(props);

        this.state = {
            searchText: null,
            sortKeys: {},
            selected: (new Map(): Map<string, boolean>)
        };

    }

    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={{ flex: 1, flexDirection: 'column', }}>
                <BackHeader navigation={this.props.navigation} buttonText={AppText.cancelButton} />
                <View style={{ flex: 1, }} >
                    <SortOptionList
                        sortOptions={SortOptions}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    selectedSort: state.sort,
});

const mapDispatchToProps = dispatch => ({
    setSort: (selectedSort) => {
        dispatch(actions.sortAndNavigate(selectedSort, RouteNames.RestaurantList));
    },
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    listItem: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#ddd',
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(SortScreen);


// export default SortScreen;

import React from 'react';
import { StackNavigator } from 'react-navigation';

import RestaurantScreen from '../screens/RestaurantScreen';
import RestaurantsScreen from '../screens/RestaurantsScreen';


export default RestaurantNavigation = StackNavigator({
  RestaurantList: { screen: RestaurantsScreen },
  RestaurantDetails: { screen: RestaurantScreen },
}, {
    headerMode: 'none',
});

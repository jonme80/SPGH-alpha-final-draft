import React from 'react';
import { StackNavigator } from 'react-navigation';

import RestaurantNavigation from './RestaurantNavigation';
import SortScreen from '../screens/SortScreen';

export default RootNavigation = StackNavigator({
  Home: { screen: RestaurantNavigation },
  Sort: { screen: SortScreen },
},{
  initialRouteName: 'Home',
  mode: 'modal',
  headerMode: 'none',
});

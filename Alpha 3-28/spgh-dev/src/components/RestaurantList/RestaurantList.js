import React from 'react';

import {
    FlatList,
    Text,
    View,
} from 'react-native';

import RestaurantListItem from '../RestaurantListItem';

class RestaurantList extends React.PureComponent {
    listItemKeyExtractor = (item, index) => item.id;

    renderListItem = ({ item }) => (
        <RestaurantListItem
            restaurant={item}
        />
    );

    render() {
        return (
            <FlatList
                data={this.props.restaurants}
                renderItem={this.renderListItem}
                keyExtractor={this.listItemKeyExtractor}
                ref={'flatListRef'}

                ListEmptyComponent={() => (
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        alignContent: 'center',
                    }}>
                        <Text>No restaurants matching the search and filter criteria applied.
                Please try again with less filters or changing your search text.</Text>
                    </View>
                )}
            />
        )
    }
}

export default RestaurantList;

import { StyleSheet } from 'react-native';

export default styles =  StyleSheet.create({
    designationPlate: {
        width: 60,
        height: 60,
    },
});

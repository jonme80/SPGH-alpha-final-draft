import { StyleSheet } from 'react-native';
import Colors from '../../styles/Colors'

export default styles = StyleSheet.create({
  linkText: {
    color: Colors.darkBlue.hex,
    fontWeight: 'bold'
  }
});
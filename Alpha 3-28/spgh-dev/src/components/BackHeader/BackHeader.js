import React from 'react';
import {
    TouchableOpacity,
    View
} from 'react-native';
import PropTypes from 'prop-types';

import _ from 'lodash';
import { LatoText } from '../StyledText';
import styles from './Styles';
import AppText from '../../constants/AppText';

const BackHeader = ({ buttonText, navigation }) => (
    <View style={styles.headerContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <LatoText style={{ fontSize: 18, paddingLeft: 5, paddingRight: 5, color: '#fff' }}>{buttonText}</LatoText>
        </TouchableOpacity>
    </View>
)

BackHeader.propTypes = {
    buttonText: PropTypes.string,
}

BackHeader.defaultProps = {
    buttonText: AppText.backButton,
}

export default BackHeader;

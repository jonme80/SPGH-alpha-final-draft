import React from 'react';
import { Text } from 'react-native';

class LatoText extends React.Component {
  render() {
    return (
      <Text
        {...this.props}
        style={[this.props.style, { fontFamily: 'latoRegular' }]}
      />
    );
  }
}

export default LatoText;
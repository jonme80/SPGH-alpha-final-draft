/*A component consisting of three different subcomponents:
- A bar spanning the component's breadth containing two text entries, one which is merely description and the second of which is contained in a TouchableHighlight and triggers the swapScreens event (see below)
- The first "screen", which displays instantly and contains anything defined by the user via a given function
- The second "screen", which display in the place of the first (and is defined similarly to it) when the swapComponent event is triggered; triggering this event again will restore the first screen, and so forth

As such, requires the following properties:
- headerText: The text which will be displayed w/n the bar at all moments, indicating the bar's function or some key point of data
- primarySwapText: The text which will be displayed in the righthand corner of the bar, which will be clicked to trigger swapComponents
- secondarySwapText: The swap text for the second screen, which will replace primary
- renderPrimaryComponent: A function called by this component which will define the contents of the first screen
- renderSecondaryComponent: A function called by this component which will define the contents of the second screen*/

import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight, Animated, UIManager, LayoutAnimation } from 'react-native'; 

export default class SwappingComponentsView extends React.Component
{
	/*Constructor*/
	constructor(props)
	{
		super(props);
		this.state = {
						headerText: this.props.headerText,
						swapText: this.props.primarySwapText,
						primarySwapText: this.props.primarySwapText,
						secondarySwapText: this.props.secondarySwapText,
						renderPrimaryComponent: this.props.renderPrimaryComponent,
						renderSecondaryComponent: this.props.renderSecondaryComponent,
						primaryStyle: this.props.primaryStyle,
						secondaryStyle: this.props.secondaryStyle,
						params: this.props.params,
						isPrimary: true
					 };
		/*Variables
		- swapText: the current text
		- primary and secondarySwapText: either possible text, will be alternated between
		- isPrimary: boolean describing whether or not we are on the primary screen*/
	}
	
	/*Swapping component methods*/
	swapComponent() //Invert isPrimary and swapText upon click, changing from one state to the other
	{
		if(this.state.isPrimary)
		{
			this.setState({
				swapText: this.state.secondarySwapText,
				isPrimary: false
			})
		}
		else
		{
			this.setState({
				swapText: this.state.primarySwapText,
				isPrimary: true
			});
		}
	}
	renderComponent() //Call the appropriate function to define the contents of the current screen based on the current state
	{
		if(this.state.isPrimary)
		{
			return (
				<View style = {this.state.primaryStyle}>
					{this.state.renderPrimaryComponent(this.state.params)}
				</View>
			);
		}
		else
		{
			return (
				<View style = {this.state.secondaryStyle}>
					{this.state.renderSecondaryComponent(this.state.params)}
				</View>
			);
		}
	}

	/*Render methods*/
	render()
	{
		/*Render:
		- bar
		- screen by function*/
		return (
			<View style={[styles.container, {flex: 3}]}>
				<View style={styles.topBar}>
					
					<View style={styles.headerContainer}>
						<Text style={styles.headerText}>{this.state.headerText}</Text>
						<TouchableHighlight onPress={this.swapComponent.bind(this)}>
							<Text style={styles.swapText}>{this.state.swapText}</Text>
						</TouchableHighlight>
					</View>
					
				</View>
				
				<View style={styles.bodyContainer}>
					{this.renderComponent()}
				</View>
			</View>
		);
	}
}

/*StyleSheet*/
var styles = StyleSheet.create({
	  container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	  },
	topBar: {
		backgroundColor: '#70abad',
		alignItems: 'center',
		flex: .175,
	},
	headerContainer: {
		flexDirection: 'row',
	},
	headerText: {
		flex: 1,
		padding: 10,
		color: '#ffffff',
		fontWeight: 'bold',
	},
	swapText: {
		flex: 1,
		padding: 10,
		color: '#ffffff',
		textDecorationLine: 'underline',
	},
	bodyContainer: {
		flex: 1,
		paddingTop: 0,
		alignSelf: 'stretch',
	},
});
/**
 * @flow
 */

import React from 'react';
import { View } from 'react-native';
import { MontserratText } from '../StyledText';
import Colors from '../../styles/Colors';
import styles from './Styles';
import BaseHeader from '../BaseHeader';
import {
    PropTypes
} from 'prop-types';
class RestaurantDetailsHeader extends React.Component {
    overlayColor = Colors[this.props.color].rgb;

    render() {
        return (
            <BaseHeader
                style={
                    [
                        styles.overlay,
                        { backgroundColor: `rgba(${this.overlayColor.r}, ${this.overlayColor.g}, ${this.overlayColor.b}, ${this.props.transparency})`}
                    ]
                }
            >
            {/*<View style={[styles.overlay, { backgroundColor: `rgba(${this.overlayColor.r}, ${this.overlayColor.g}, ${this.overlayColor.b}, ${this.props.transparency})`, }]}>*/}
                <MontserratText style={styles.restaurantTitle}>
                    {this.props.text}
                </MontserratText>
            </BaseHeader>
            // </View>
        )
    }
};

RestaurantDetailsHeader.PropTypes = {
    text: PropTypes.string,
    transparency: PropTypes.string,
    color: PropTypes.string,
}

RestaurantDetailsHeader.defaultProps = {
    transparency: 0.85,
    color: 'seafoamBlue'
}

export default RestaurantDetailsHeader;

import React from 'react';
import {
    Image,
    StyleSheet,
    TextInput,
    TouchableWithoutFeedback,
    TouchableOpacity,
    View,
    Keyboard
} from 'react-native';

import {
    MaterialIcons
} from '@expo/vector-icons';

import {
    LatoText
} from '../StyledText';

import Styles from './Styles';
import BaseHeader from '../BaseHeader';
import Colors from '../../styles/Colors';
import AppText from '../../constants/AppText';


class RestaurantsScreenHeader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            enteringSearchInput: false,
        };

        this.handleSearchTextInputChange = this.handleSearchTextInputChange.bind(this);
		this.handleAboutButtonClick = this.handleAboutButtonClick.bind(this);
        this.handleSortButtonClick = this.handleSortButtonClick.bind(this);
    }

	handleAboutButtonClick(e) {
		this.props.setAbout();
	}
	
    handleSortButtonClick(e) {
        this.props.setSort();
    }

    handleSearchTextInputChange(e) {
        if (e.nativeEvent.key === "Enter") {
            Keyboard.dismiss();
        }
        this.props.setSearchText(e.nativeEvent.text);
    }

    clearSearchState = () => {
        this.props.setSearchText("");

        this.setState({
            enteringSearchInput: false,
        });
    }

    render() {
        if (this.state.enteringSearchInput) {
            return (
                // <View style={[this.props.style, Styles.headerContainer]}>
                <BaseHeader>
                    <View style={Styles.searchBar}>
                        <TextInput
                            style={Styles.searchText}
                            value={this.props.searchText}
                            onChange={this.handleSearchTextInputChange}
                            placeholder={AppText.searchPlaceholder}
                            autoFocus={true}
                            returnKeyType={'done'}
                            onSubmitEditing={() => Keyboard.dismiss()}
                            underlineColorAndroid='transparent'
                            onBlur={
                                () => {
                                    this.setState({
                                        enteringSearchInput: false,
                                    })
                                }
                            }
                        />
                        <TouchableWithoutFeedback onPress={this.clearSearchState}>
                            <MaterialIcons
                                name={'cancel'}
                                size={18}
                                color={Colors.gold.hex}
                            />
                        </TouchableWithoutFeedback>
                    </View>
                    <TouchableOpacity onPress={this.clearSearchState}>
                        <LatoText style={{ fontSize: 18, paddingLeft: 5, paddingRight: 5, color: '#fff' }}>{AppText.cancelButton}</LatoText>
                    </TouchableOpacity>
                </BaseHeader>
            )
        }
        return (
            <BaseHeader>
                <TouchableOpacity onPress={this.handleAboutButtonClick}>
					<Image
						source={require('../../../assets/icons/spr-logo-250px.png')}
						style={Styles.navigationLogo}
					/>
				</TouchableOpacity>
                <TextInput
                    style={[Styles.searchText, Styles.searchBar]}
                    value={this.props.searchText}
                    onChange={this.handleSearchTextInputChange}
                    placeholder={AppText.searchPlaceholder}
                    underlineColorAndroid='transparent'                    
                    onFocus={() => {
                        this.setState({ enteringSearchInput: true })
                    }
                    }
                />
                <TouchableOpacity onPress={this.handleSortButtonClick}>
                    <LatoText style={{ fontSize: 18, paddingLeft: 5, paddingRight: 5, color: '#fff' }}>{this.props.sortTitle}</LatoText>
                </TouchableOpacity>
            </BaseHeader>

        )
    }
}

export default RestaurantsScreenHeader;

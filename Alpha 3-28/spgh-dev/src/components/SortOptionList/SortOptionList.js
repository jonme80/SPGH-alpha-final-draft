import React from 'react';

import {
    FlatList,
} from 'react-native';

import SortOptionListItem from '../SortOptionListItem';

const listItemKeyExtractor = (item, index) => item.id;

const renderListItem = ({ item }) => (
    <SortOptionListItem
        id={item.id}
        sortOption={item}
    />
);

const SortOptionList = ({sortOptions}) => (
    <FlatList
        data={sortOptions}
        renderItem={renderListItem}
        keyExtractor={listItemKeyExtractor}
    />
)

export default SortOptionList;

import { Platform } from 'react-native';

const PlatformMapsUri = (address) => {
    const baseUri = Platform.OS === 'ios' ? 'http://maps.apple.com/?address=' : 'https://www.google.com/maps/search/?api=1&query=';

    return `${baseUri}${address}`;
}

export default PlatformMapsUri;